# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
	index_prim_impar = Int(n * (n - 1) / 2)
	prim_impar = index_prim_impar * 2 + 1
	return prim_impar
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
	print(string(m) * " " * string(m^3) * " ")

	prim_impar = impares_consecutivos(m)
	for i = 0:(m - 1)
		impar_atual = prim_impar + i * 2
		print(string(impar_atual) * (if (i === m) "" else " " end))
	end
end

function mostra_n(n)
    for i = 1:n
		imprime_impares_consecutivos(i)
		println()
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
# test()